# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - mai 08, 2022
### Recycle
- :recycle: refactoring in vacancy features
- :recycle: refactoring in enrolled features
- :recycle: refactoring in candidate features
- :truck: change folder architecture

## [1.0.2] - mai 07, 2022
### Added
- :sparkles: initial implementation of CRUD enrolled
- :sparkles: many-to-one join of candidate enrolled tables
- :sparkles: CRUD implemented for vacancy
- :sparkles: vacancy's initial implementation

## [1.0.1] - mai 06, 2022
### Added
- :sparkles: CRUD implemented for candidate

## [1.0.1] - mai 05, 2022
### Added
- :tada: candidate's initial implementation
