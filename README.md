<div align="center">
    <h1>Job applicants</h1>
    <p align="center">API to track job applicants and job vacancies</p>
</div>

### 📁 Index
- [Goal](https://gitlab.com/JeanMalavasi/nodejs_jobs#%EF%B8%8Fgoal)
- [Prerequisites](https://gitlab.com/JeanMalavasi/nodejs_jobs#%EF%B8%8Fprerequisites)
- [Preparing the database](https://gitlab.com/JeanMalavasi/nodejs_jobs#preparing-the-database)
- [Last adjustments](https://gitlab.com/JeanMalavasi/nodejs_jobs#last-adjustments)
- [Running server](https://gitlab.com/JeanMalavasi/nodejs_jobs#running-server)
- [Technologies](https://gitlab.com/JeanMalavasi/nodejs_jobs#technologies)
- [Enviroment variables](https://gitlab.com/JeanMalavasi/nodejs_jobs#enviroment-variables)
- [Functionalities](https://gitlab.com/JeanMalavasi/nodejs_jobs#technologies)

### ✔️Goal
API developed to manage job applicants.

Allows a CRUD of candidates, vacancies, enrollment

### ✔️Prerequisites
Before starting, the environment must have the following tools installed.
<div>
	<a href="https://nodejs.org/en/">NodeJs</a> • <a href="https://www.postgresql.org/download/">Postegresql</a>
</div>

### 🔨Preparing the database
##### Windows enviroment
1. After cloning the project, run ```npm install```
2. Open terminal and run ```psql -U postgres```
3. Run the command```CREATE USER `{seu_usuario}` WITH ENCRYPTED PASSWORD '{sua_senha}' CREATEDB;```
4. Create a file ```.env``` in the root of the project containing the following line ```DATABASE_URL=postgres://{database_user}:{database_password}@localhost:{database_port}/{database_name}}```
5. Run the migrations ```npx sequelize-cli db:migrate```
6. Run the seeders ```npx sequelize-cli db:seed:all```

### 🔨Last adjustments
1. In the file ```.env``` add the following line```SERVER_PORT=8080```

### 🎉Running server
1. Open the terminal and run ```npm run dev```

### 🔧Technologies
The following tools were used in building the project
- [NodeJs](https://nodejs.org/en/)
- [Postegresql](https://www.postgresql.org/download/)
- [Sequelize](https://sequelize.org/)
- [Express](https://expressjs.com/pt-br/)

### 🔥Enviroment variables
```
SERVER_PORT=8080
DATABASE_URL=postgres://{database_user}:{database_password}@localhost:{database_port}/{database_name}
``` 

### 📝Functionalities
<details><summary><strong>Candidate</strong></summary>

✅ Search all candidates

✅ Search for a candidate by id

✅ Search for a candidate by email

✅ Create new candidate

✅ Update a candidate by id

✅ Delete a candidate by id

</details>

<details><summary><strong>Vacancy</strong></summary>

✅ Search all vacancies

✅ Search for a vacancy by id

✅ Create new vacancy

✅ Update a vacancy by id

✅ Delete a vacancy by id

</details>

<details><summary><strong>Enrolled</strong></summary>

✅ Search all enrolleds 

✅ Search for a enrolled by id 

✅ Search for a enrolled by email

✅ Create new enrolled 

✅ Update a enrolled by id 

✅ Delete a enrolled by id

</details>
