import { VacancyErros } from "../../enums/errors/Vacancy"
import { ValidateResponseRepositorie } from "../../helpers/ValidateResponse"
import { Vacancy } from "../entities"
import { VacancyIn } from "../models/in/Vacancy"

export default {
    getAll: async () => {
        return await Vacancy.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            }
        })
    },
    getOneById: async (id: string) => {
        const hasVacancy = await Vacancy.findByPk(id, {
            include: 'enrolleds'
        })

        return hasVacancy ?
            hasVacancy :
            { errors: VacancyErros.NOT_FOUND_BY_ID }
    },
    getOneByEmail: async function (email: string) {
        const hasVacancy = await Vacancy.findOne({ where: { "email": email }, include: 'enrolleds' })

        return hasVacancy ?
            hasVacancy :
            { errors: VacancyErros.NOT_FOUND_BY_EMAIL }
    },
    createOne: async function (vacancy: VacancyIn) {
        return await Vacancy.create(vacancy)
    },
    updateOneById: async function (id: string, vacancy: VacancyIn) {
        const hasVacancy = ValidateResponseRepositorie(await this.getOneById(id))

        if (hasVacancy) {
            return await Vacancy.update(vacancy, { where: { "id": id } })
        }
        return { errors: VacancyErros.NOT_FOUND_BY_ID }
    },
    deleteOneById: async function (id: string) {
        const hasVacancy = ValidateResponseRepositorie(await this.getOneById(id))

        return hasVacancy ?
            await Vacancy.destroy({ where: { "id": id } }) :
            { errors: VacancyErros.NOT_FOUND_BY_ID }
    }
    // updateOne: async (vacancy: VacancyIn, id: string) => {
    //     return await Vacancy.update(vacancy, { where: { "id": id } })
    // },
    // deleteOne: async (id: string) => {
    //     return await Vacancy.destroy({ where: { "id": id } })
    // }
}