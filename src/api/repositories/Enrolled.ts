import { CandidateErros } from "../../enums/errors/Candidate";
import { EnrolledErros } from "../../enums/errors/Enrolled";
import { VacancyErros } from "../../enums/errors/Vacancy";
import { ValidateResponseRepositorie } from "../../helpers/ValidateResponse";
import { Enrolled } from "../entities"
import { EnrolledIn } from "../models/in/Enrolled";
import Candidate from "./Candidate";
import Vacancy from "./Vacancy";

export default {
    getAll: async function () {
        return await Enrolled.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            }
        });
    },
    getOneById: async function (id: string) {
        const hasEnrolled = await Enrolled.findByPk(id, { include: ['candidate', 'vacancy'] })

        return hasEnrolled ?
            hasEnrolled :
            { erros: EnrolledErros.NOT_FOUND_BY_ID }
    },
    createOne: async function (enrolled: EnrolledIn) {
        const hasCandidate = ValidateResponseRepositorie(await Candidate.getOneById(enrolled.candidateId))
        const hasVacancy = ValidateResponseRepositorie(await Vacancy.getOneById(enrolled.vacancyId))
        
        return hasCandidate ?
            hasVacancy ?
                await Enrolled.create(enrolled) :
                { erros: VacancyErros.NOT_FOUND_BY_ID } :
            { erros: CandidateErros.NOT_FOUND_BY_ID }
    },

    deleteOneById: async function (id: string) {
        const hasEnrolled = ValidateResponseRepositorie(await this.getOneById(id))

        return hasEnrolled ?
            await Enrolled.destroy({ where: { 'id': id } }) :
            { errors: EnrolledErros.NOT_FOUND_BY_ID }
    }
}