import { CandidateErros } from '../../enums/errors/Candidate';
import { ValidateResponseRepositorie } from '../../helpers/ValidateResponse';
import { Candidate } from '../entities';
import { CandidateIn } from '../models/in/Candidate';

export default {
    getAll: async function () {
        return await Candidate.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            }
        })
    },

    getOneById: async function (id: string) {
        const hasCandidate = await Candidate.findByPk(id, {
            include: 'enrolleds'
        })

        return hasCandidate ?
            hasCandidate :
            { errors: CandidateErros.NOT_FOUND_BY_ID }
    },

    getOneByEmail: async function (email: string) {
        const hasCandidate = await Candidate.findOne({ where: { "email": email }, include: 'enrolleds' })

        return hasCandidate ?
            hasCandidate :
            { errors: CandidateErros.NOT_FOUND_BY_EMAIL }
    },

    createOne: async function (candidate: CandidateIn) {
        const hasCandidate = ValidateResponseRepositorie(await this.getOneByEmail(candidate.email))

        return hasCandidate ?
            { errors: CandidateErros.EMAIL_UNIQUE } :
            await Candidate.create(candidate)
    },

    updateOneById: async function (id: string, candidate: CandidateIn) {
        const hasCandidate = ValidateResponseRepositorie(await this.getOneById(id))

        if (hasCandidate) {
            const hasEmail = ValidateResponseRepositorie(await this.getOneByEmail(candidate.email))
            if (hasEmail)
                return { errors: CandidateErros.EMAIL_UNIQUE }
            return await Candidate.update(candidate, { where: { "id": id } })
        }
        return { errors: CandidateErros.NOT_FOUND_BY_ID }
    },

    deleteOneById: async function (id: string) {
        const hasCandidate = ValidateResponseRepositorie(await this.getOneById(id))

        return hasCandidate ?
            await Candidate.destroy({ where: { "id": id } }) :
            { errors: CandidateErros.NOT_FOUND_BY_ID }
    }
}