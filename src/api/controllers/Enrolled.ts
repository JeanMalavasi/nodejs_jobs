import express from "express";
import { EnrolledIn } from "../models/in/Enrolled";
import EnrolledUC from "../services/Enrolled";

export const route = express.Router()

route.get('/', async (req, res) => {
    try {
        const enrolleds = await EnrolledUC.getAll()
        res.status(200).json(enrolleds)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.get('/:id', async (req, res) => {
    const { id } = req.params

    try {
        const enrolled = await EnrolledUC.getOneById(id)
        return res.status(enrolled.status).json(enrolled.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.post('/', async (req, res) => {
    const enrolledIn: EnrolledIn = req.body

    try {
        const enrolled = await EnrolledUC.createOne(enrolledIn)
        return res.status(enrolled.status).json(enrolled.response) 
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.delete('/:id', async (req, res) => {
    const { id } = req.params
    
    try {
        const enrolled = await EnrolledUC.deleteOneById(id)        
        return res.status(enrolled.status).json(enrolled.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})
