import express from "express";
import { VacancyIn } from "../models/in/Vacancy";
import VacancyUC from "../services/Vacancy";

export const route = express.Router()

route.get('/', async (req, res) => {
    try {
        const vacancies = await VacancyUC.getAll()
        res.status(200).json(vacancies)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.get('/id/:id', async (req, res) => {
    const { id } = req.params

    try {
        const vacancy = await VacancyUC.getOneById(id)
        res.status(vacancy.status).json(vacancy.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.get('/email/:email', async (req, res) => {
    const { email } = req.params

    try {
        const candidate = await VacancyUC.getOneByEmail(email)
        res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.post('/', async (req, res) => {
    const vacancyIn: VacancyIn = req.body

    try {
        const vacancy = await VacancyUC.createOne(vacancyIn)
        res.status(vacancy.status).json(vacancy.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.put('/:id', async (req, res) => {
    const vacancyIn: VacancyIn = req.body
    const { id } = req.params
    
    try {
        const vacancy = await VacancyUC.updateOneById(id, vacancyIn)        
        res.status(vacancy.status).json(vacancy.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.delete('/:id', async (req, res) => {
    const { id } = req.params
    
    try {
        const vacancy = await VacancyUC.deleteOneById(id)        
        return res.status(vacancy.status).json(vacancy.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})