import express from "express";
import { CandidateIn } from "../models/in/Candidate";
import CandidateUC from "../services/Candidate";

export const route = express.Router()

route.get('/', async (req, res) => {
    try {
        const candidates = await CandidateUC.getAll()
        res.status(200).json(candidates)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.get('/id/:id', async (req, res) => {
    const { id } = req.params

    try {
        const candidate = await CandidateUC.getOneById(id)
        res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.get('/email/:email', async (req, res) => {
    const { email } = req.params

    try {
        const candidate = await CandidateUC.getOneByEmail(email)
        res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.post('/', async (req, res) => {
    const candidateIn: CandidateIn = req.body

    try {
        const candidate = await CandidateUC.createOne(candidateIn)
        res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.put('/:id', async (req, res) => {
    const candidateIn: CandidateIn = req.body
    const { id } = req.params
    
    try {
        const candidate = await CandidateUC.updateOneById(id, candidateIn)        
        res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

route.delete('/:id', async (req, res) => {
    const { id } = req.params
    
    try {
        const candidate = await CandidateUC.deleteOneById(id)        
        return res.status(candidate.status).json(candidate.response)
    } catch (e) {
        res.status(500).json({error: e.message})
    }
})

