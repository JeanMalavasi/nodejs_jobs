import { ClassName } from '../../enums/ClassName'
import { ValidateResponseService } from '../../helpers/ValidateResponse'
import { CandidateMapper } from '../../mappers/candidate'
import { CandidateIn } from '../models/in/Candidate'
import CandidateRepo from '../repositories/Candidate'


export default {
    getAll: async () => {
        const res = (await CandidateRepo.getAll()).map(CandidateMapper.fromDomain)
        return res
    },
    getOneById: async (id: string) => {
        const res = await CandidateRepo.getOneById(id)
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    },
    getOneByEmail: async (email: string) => {
        const res = await CandidateRepo.getOneByEmail(email)
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    },
    createOne: async (candidate: CandidateIn) => {
        const res = await CandidateRepo.createOne(CandidateMapper.toDomain(candidate))
        return ValidateResponseService(res, 201, 400, ClassName.CANDIDATE)
    },
    updateOneById: async (id: string, candidate: CandidateIn) => {
        const res = await CandidateRepo.updateOneById(id, CandidateMapper.toDomain(candidate))
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    },
    deleteOneById: async (id: string) => {
        const res = await CandidateRepo.deleteOneById(id)
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    }
}