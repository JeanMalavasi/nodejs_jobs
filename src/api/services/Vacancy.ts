import { VacancyMapper } from "../../mappers/Vacancy"
import { VacancyIn } from "../models/in/Vacancy"
import VacancyRepo from "../repositories/Vacancy"
import { ValidateResponseService } from '../../helpers/ValidateResponse'
import { ClassName } from "../../enums/ClassName"

export default {
    getAll: async () => {
        const res = (await VacancyRepo.getAll()).map(VacancyMapper.fromDomain)
        return res
    },
    getOneById: async (id: string) => {
        const res = await VacancyRepo.getOneById(id)
        return ValidateResponseService(res, 200, 404, ClassName.VACANCY)
    },
    getOneByEmail: async (email: string) => {
        const res = await VacancyRepo.getOneByEmail(email)
        return ValidateResponseService(res, 200, 404, ClassName.VACANCY)
    },
    createOne: async (vacancy: VacancyIn) => {
        const res = await VacancyRepo.createOne(VacancyMapper.toDomain(vacancy))
        return ValidateResponseService(res, 201, 400, ClassName.CANDIDATE)
    },
    updateOneById: async (id: string, vacancy: VacancyIn) => {
        const res = await VacancyRepo.updateOneById(id, VacancyMapper.toDomain(vacancy))
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    },
    deleteOneById: async (id: string) => {
        const res = await VacancyRepo.deleteOneById(id)
        return ValidateResponseService(res, 200, 404, ClassName.CANDIDATE)
    }
}