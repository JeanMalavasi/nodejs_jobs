import { ClassName } from "../../enums/ClassName"
import { ValidateResponseService } from "../../helpers/ValidateResponse"
import { EnrolledMapper } from "../../mappers/Enrolled"
import { EnrolledIn } from "../models/in/Enrolled"
import EnrolledRepo from "../repositories/Enrolled"

export default {
    getAll: async () => {
        const res = (await EnrolledRepo.getAll()).map(EnrolledMapper.fromDomain)
        return res
    },
    getOneById: async (id: string) => {
        const res = await EnrolledRepo.getOneById(id)
        return  ValidateResponseService(res, 200, 404, ClassName.ENROLLED)
    },
    createOne: async (enrolled: EnrolledIn) => {
        const res = await EnrolledRepo.createOne(EnrolledMapper.toDomain(enrolled))
        return ValidateResponseService(res, 201, 400, ClassName.ENROLLED)
    },
    deleteOneById: async (id: string) => {
        const res = await EnrolledRepo.deleteOneById(id)
        return ValidateResponseService(res, 200, 404, ClassName.ENROLLED)
    }
}