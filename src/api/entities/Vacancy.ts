import { DataTypes, Model } from "sequelize";
import { sequelize } from "../../database";

export interface VacancyInstance extends Model {
  id: number
  name: string
  company: string
  email: string
  description: string
}

export default sequelize.define<VacancyInstance>('vacancy',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    company: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: DataTypes.TEXT,
  })