import Candidate from "./Candidate"
import Enrolled from "./Enrolled"
import Vacancy from "./Vacancy"

Candidate.hasMany(Enrolled)
Enrolled.belongsTo(Candidate)

Vacancy.hasMany(Enrolled)
Enrolled.belongsTo(Vacancy)

export {
    Candidate,
    Enrolled,
    Vacancy
}