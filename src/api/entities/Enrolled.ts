import { sequelize } from '../../database'
import { DataTypes, Model } from 'sequelize'

export interface EnrolledInstance extends Model {
    id: number
    candidateId: number
    vacancyId: number
}

export default sequelize.define<EnrolledInstance>('enrolled',
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        candidateId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'candidate',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        vacancyId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'vacancy',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        }
    }
)

