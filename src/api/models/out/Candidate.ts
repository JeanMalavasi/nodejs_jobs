import { EnrolledOut } from "./Enrolled"

export type CandidateOut = {
    id: number
    name: string
    email: string
    bio: string
    phone: string
    openToWork: boolean
    createdAt: string
    updatedAt: string
    enrolleds: EnrolledOut
}