import { EnrolledOut } from "./Enrolled"

export type VacancyOut = {
    id: number
    name: string
    company: string
    email: string
    description: string
    createdAt: string
    updatedAt: string
    enrolleds: EnrolledOut
}