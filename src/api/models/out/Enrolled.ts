import { CandidateOut } from "./Candidate"
import { VacancyOut } from "./Vacancy"

export type EnrolledOut = {
    id: number
    candidateId: number
    vacancyId: number
    candidate: CandidateOut
    vacancy: VacancyOut
    createdAt: string
    updatedAt: string
}