export type CandidateIn = {
    name: string
    bio: string
    email: string
    phone: string
    openToWork: boolean
}