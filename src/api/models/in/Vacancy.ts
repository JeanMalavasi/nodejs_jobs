export type VacancyIn = {
    name: string
    company: string
    email: string
    description: string
}