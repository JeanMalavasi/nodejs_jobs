export type EnrolledIn = {
    candidateId: string
    vacancyId: string
}