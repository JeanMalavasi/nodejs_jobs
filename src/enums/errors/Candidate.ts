export enum CandidateErros {
    NOT_FOUND_BY_ID = "Candidate ID not found",
    NOT_FOUND_BY_EMAIL = "Candidate EMAIL not found",
    EMAIL_UNIQUE = "Candidate EMAIL must be unique"
}