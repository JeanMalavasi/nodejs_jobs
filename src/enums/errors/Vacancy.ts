export enum VacancyErros {
    NOT_FOUND_BY_ID = "Vacancy ID not found",
    NOT_FOUND_BY_EMAIL = "Vacancy EMAIL not found",
}