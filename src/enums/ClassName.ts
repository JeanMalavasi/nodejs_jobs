export enum ClassName {
    CANDIDATE = "candidate",
    ENROLLED = "enrolled",
    VACANCY = "vacancy"
}