import { CandidateInstance } from "../api/entities/Candidate"
import { CandidateIn } from "../api/models/in/Candidate"
import { CandidateOut } from "../api/models/out/Candidate"

export class CandidateMapper {
    public static toDomain(candidate: CandidateIn): CandidateIn  {
        return {
            name: candidate.name,
            bio: candidate.bio,
            email: candidate.email,
            phone: candidate.phone,
            openToWork: candidate.openToWork
        }
    }

    public static fromDomain(candidate: CandidateInstance): CandidateOut {
        return {
            id: candidate.id,
            email: candidate.email,
            name: candidate.name,
            bio: candidate.bio,
            phone: candidate.phone,
            openToWork: candidate.openToWork,
            createdAt: candidate.getDataValue('createdAt'),
            updatedAt: candidate.getDataValue('updatedAt'),
            enrolleds: candidate.getDataValue('enrolleds')
        }

    }
}