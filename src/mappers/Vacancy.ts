import { VacancyInstance } from "../api/entities/Vacancy"
import { VacancyIn } from "../api/models/in/Vacancy"

export class VacancyMapper {
    public static toDomain(vacancy: VacancyIn): VacancyIn {
        return {
            name: vacancy.name,
            company: vacancy.company,
            email: vacancy.email,
            description: vacancy.description
        }
    }

    public static fromDomain(vacancy: VacancyInstance) {
        return vacancy
    }
}