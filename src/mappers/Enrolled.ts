import { EnrolledInstance } from "../api/entities/Enrolled"
import { EnrolledIn } from "../api/models/in/Enrolled"
import { VacancyIn } from "../api/models/in/Vacancy"
import { EnrolledOut } from "../api/models/out/Enrolled"

export class EnrolledMapper {
    public static toDomain(enrolled: EnrolledIn): EnrolledIn {
        return {
            candidateId: enrolled.candidateId,
            vacancyId: enrolled.vacancyId
        }
    }

    public static fromDomain(enrolled: EnrolledInstance): EnrolledOut {
       
        return {
            id: enrolled.id,
            candidateId: enrolled.candidateId,
            vacancyId: enrolled.vacancyId,
            createdAt: enrolled.getDataValue('createdAt'),
            updatedAt: enrolled.getDataValue('updatedAt'),
            candidate: enrolled.getDataValue('candidate'),
            vacancy: enrolled.getDataValue('vacancy'),
        }
    }
}