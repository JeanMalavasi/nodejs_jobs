'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const [candidates] = await queryInterface.sequelize.query('SELECT id FROM candidate;');
    const [vacancys] = await queryInterface.sequelize.query('SELECT id FROM vacancy;');

    await queryInterface.bulkInsert('enrolled',
      candidates.map((candidate, index) => (
        {
          candidate_id: candidate.id,
          vacancy_id: vacancys[index].id,
          created_at: new Date(),
          updated_at: new Date()
        }
      ))
    )
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('enrolled', null, {});
  }
};
