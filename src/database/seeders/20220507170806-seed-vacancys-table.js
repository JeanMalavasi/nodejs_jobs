'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('vacancy', [{
      name: 'Front-end',
      company: 'Companie A',
      email: 'companieA@hotmail.com',
      description: 'Vacaines for front-end developer.',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      name: 'Back-end',
      company: 'Companie B',
      email: 'companieB@outlook.com',
      description: 'Vacaines for back-end developer.',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      name: 'Infra',
      company: 'Companie C',
      email: 'companieC@gmail.com',
      description: 'Vacaines for infra developer.',
      created_at: new Date(),
      updated_at: new Date()
    }])
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('vacancy', null, {});
  }
};
