import { Sequelize } from "sequelize";

export const sequelize = new Sequelize(process.env.DATABASE_URL, {
    define: {
        underscored: true,
        freezeTableName: true
    },
})