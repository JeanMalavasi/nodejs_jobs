import express from 'express'

import { route as Candidate } from './api/controllers/Candidate'
import { route as Vacancy } from './api/controllers/Vacancy'
import { route as Enrolled } from './api/controllers/Enrolled'

const app = express()

app.use(express.json())
app.use('/candidate', Candidate)
app.use('/vacancy', Vacancy)
app.use('/enrolled', Enrolled)

app.listen(process.env.SERVER_PORT, () => {
    console.log('Servidor ouvindo na porta ' + process.env.SERVER_PORT);
})