import { CandidateInstance } from "../api/entities/Candidate";
import { EnrolledInstance } from "../api/entities/Enrolled";
import { VacancyInstance } from "../api/entities/Vacancy";
import { ClassName } from "../enums/ClassName";
import { CandidateMapper } from "../mappers/candidate";
import { EnrolledMapper } from "../mappers/Enrolled";
import { VacancyMapper } from "../mappers/Vacancy";

export function MapperResponse<RES>(res: RES, className: string) {
    if(res.hasOwnProperty('dataValues')) {
        switch (className) {
            case ClassName.CANDIDATE:
                return CandidateMapper.fromDomain(res as unknown as CandidateInstance)
            case ClassName.ENROLLED:
                return EnrolledMapper.fromDomain(res as unknown as EnrolledInstance)
            case ClassName.VACANCY:
                return VacancyMapper.fromDomain(res as unknown as VacancyInstance)
            default:
                break;
        }
    }
    return res
}