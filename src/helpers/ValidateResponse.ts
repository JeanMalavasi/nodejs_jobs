import { MapperResponse } from "./MapperResponse";

export function ValidateResponseService<RES>(res: RES, statusOK: number, statuesError: number, className: string) {
    return res.hasOwnProperty('errors') ?
        { status: statuesError, response: res } :
        (
            {
                status: statusOK,
                response: MapperResponse(res, className)  
            }
        )
}

export function ValidateResponseRepositorie<RES>(res: RES) {
    return res.hasOwnProperty('errors') ?
        false :
        true
}